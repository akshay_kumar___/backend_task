 var mongoose = require('mongoose');
 var jwt = require('jsonwebtoken');
 var session = require('session');
 var User = mongoose.model('user');
 var Session = mongoose.model('session');
 var config = require('config');
 /* the response object for API
   error : true / false 
   code : contains any error code
   data : the object or array for data
   userMessage : the message for user, if any.
 */

 var response = {
   error: false,
   status: 200,
   data: null,
   userMessage: '',
   errors: null
 };

 var NullResponseValue = function() {
   response = {
     error: false,
     status: 200,
     data: null,
     userMessage: '',
     errors: null
   };
   return true;
 };
 var SendResponse = function(res, status) {
   res.status(status || 200).send(response);
   NullResponseValue()
   return
 };

 var methods = {};

 /*
 Routings/controller goes here
 */
 module.exports.controller = function(router) {

   router
     .route('/users')
     .post(methods.createNewUser)
     .put(session.checkToken, methods.updateUser)

   router
     .route('/login')
     .post(methods.loginUser)

   router
     .route('/logout')
     .delete(session.checkToken, methods.logoutUser)

   router
     .route('/ping')
     .get(session.checkToken, methods.pingUser)


 };
 /*=================================
 ***   api for register login  ***
 ===================================*/

 /*===========================
 ***   create new user  ***
 =============================*/

 methods.createNewUser = function(req, res) {
   //Check for POST request errors.
   req.checkBody('name', 'name code is required.').notEmpty();
   req.checkBody('email', 'email code is required.').notEmpty();
   req.checkBody('password', 'password code is required.').notEmpty();
   req.checkBody('dob', 'dob must be dd/mm/yyyy').isDate();
   var errors = req.validationErrors(true);
   if (errors) {
     response.error = true;
     response.status = 400;
     response.errors = errors;
     response.userMessage = 'Validation errors';
     return SendResponse(res);
   } else {
     //Database functions here
     User.findOne({
         email: req.body.email
       })
       .lean()
       .exec(function(err, user) {
         if (err) {
           //send response to client
           response.error = true;
           response.status = 500;
           response.errors = err;
           response.userMessage = 'server error';
           response.data = null;
           return SendResponse(res);
         } else if (user) {
           //send response to client
           response.error = true;
           response.status = 400;
           response.errors = null;
           response.userMessage = 'email already exists';
           response.data = null;
           return SendResponse(res);
         } else {
           user = new User({
             name: req.body.name,
             email: req.body.email,
             password: req.body.password,
             dob: req.body.dob,
             online: true
           });
           user.save(function(err) {
             if (err) {
               //send response to client
               response.error = true;
               response.status = 500;
               response.errors = err;
               response.userMessage = 'server error';
               response.data = null;
               return SendResponse(res);
             } else {
               var token = jwt.sign({
                 email: req.body.email
               }, config.sessionSecret, {
                 expiresIn: 60 * 120
               });
               var newSession = new Session({
                 authToken: token,
                 userId: user._id
               });
               newSession.save(function(err) {
                 if (err) {
                   //send response to client
                   response.error = true;
                   response.status = 500;
                   response.errors = err;
                   response.userMessage = 'server error';
                   response.data = null;
                   return SendResponse(res);
                 } else {
                   //send response to client
                   response.error = false;
                   response.status = 200;
                   response.errors = null;
                   response.userMessage = 'user registred successfuly.';
                   response.data = {
                     userId: user._id,
                     authToken: token
                   };
                   return SendResponse(res);
                 }
               });
             }
           });
         }
       });
   }
 }

 /*-----  End of createNewUser  ------*/

 /*=====================
 ***   ping user  ***
 =======================*/

 methods.pingUser = function(req, res) {
   //Check for POST request errors.
   //send response to client
   response.error = false;
   response.status = 200;
   response.errors = null;
   response.userMessage = 'pong';
   response.data = req.user;
   return SendResponse(res);
 };

 /*-----  End of pingUser  ------*/

 /*======================
 ***   login user  ***
 ========================*/

 methods.loginUser = function(req, res) {
   //Check for POST request errors.
   req.checkBody('email', 'email code is required.').notEmpty();
   req.checkBody('password', 'password code is required.').notEmpty();
   var errors = req.validationErrors(true);
   if (errors) {
     response.error = true;
     response.status = 400;
     response.errors = errors;
     response.userMessage = 'Validation errors';
     return SendResponse(res);
   } else {
     //Database functions here
     User.findOne({
       email: req.body.email,
       password: req.body.password
     }, function(err, user) {
       if (err) {
         //send response to client
         response.error = true;
         response.status = 500;
         response.errors = err;
         response.userMessage = 'server error';
         response.data = null;
         return SendResponse(res);
       } else if (!user) {
         //send response to client
         response.error = true;
         response.status = 400;
         response.errors = null;
         response.userMessage = 'email and password combination is incorrect.';
         response.data = null;
         return SendResponse(res);
       } else {
         var token = jwt.sign({
           email: req.body.email
         }, config.sessionSecret, {
           expiresIn: 60 * 120
         });
         Session.findOneAndUpdate({
             userId: user._id
           }, {
             authToken: token,
             createdAt: new Date()
           })
           .exec(function(err, user_session) {
             if (err) {
               //send response to client
               response.error = true;
               response.status = 500;
               response.errors = err;
               response.userMessage = 'server error';
               response.data = null;
               return SendResponse(res);
             } else if (!user_session) {
               var newSession = new Session({
                 authToken: token,
                 userId: user._id
               });
               newSession.save(function(err) {
                 if (err) {
                   //send response to client
                   response.error = true;
                   response.status = 500;
                   response.errors = err;
                   response.userMessage = 'server error';
                   response.data = null;
                   return SendResponse(res);
                 } else {
                   user.online = true;
                   user.save(function(err) {
                     if (err) {
                       //send response to client
                       response.error = true;
                       response.status = 500;
                       response.errors = err;
                       response.userMessage = 'server error';
                       response.data = null;
                       return SendResponse(res);
                     } else {
                       //send response to client
                       response.error = false;
                       response.status = 200;
                       response.errors = null;
                       response.userMessage = 'login success';
                       response.data = {
                         userId: user._id,
                         authToken: token
                       };
                       return SendResponse(res);

                     }
                   });
                 }
               });
             } else {
               user.online = true;
               user.save(function(err) {
                 if (err) {
                   //send response to client
                   response.error = true;
                   response.status = 500;
                   response.errors = err;
                   response.userMessage = 'server error';
                   response.data = null;
                   return SendResponse(res);
                 } else {
                   //send response to client
                   response.error = false;
                   response.status = 200;
                   response.errors = null;
                   response.userMessage = 'login success';
                   response.data = {
                     userId: user._id,
                     authToken: token
                   };
                   return SendResponse(res);

                 }
               });
             }
           });
       }
     })
   }
 }

 /*-----  End of loginUser  ------*/

 /*=======================
 ***   logout user  ***
 =========================*/

 methods.logoutUser = function(req, res) {
   //Check for POST request errors.
   Session.remove({
     userId: req.user._id
   }, function(err) {
     if (err) {
       //send response to client
       response.error = tru;
       response.status = 500;
       response.errors = err;
       response.userMessage = 'server errors.';
       response.data = null;
       return SendResponse(res);
     } else {
       req.user.online = false;
       req.user.save(function(err) {
         if (err) {
           //send response to client
           response.error = tru;
           response.status = 500;
           response.errors = err;
           response.userMessage = 'server errors.';
           response.data = null;
           return SendResponse(res);
         } else {
           //send response to client
           response.error = false;
           response.status = 200;
           response.errors = null;
           response.userMessage = 'logout success';
           response.data = null;
           return SendResponse(res);
         }
       });
     }
   })
 }

 /*-----  End of logoutUser  ------*/

 /*=======================
 ***   update user  ***
 =========================*/

 methods.updateUser = function(req, res) {
   //Check for POST request errors.
   //update user using keys into user schema
   for (key in req.body) {
     req.user[key] = req.body[key];
   }
   req.user.save(function(err) {
     if (err) {
       //send response to client
       response.error = true;
       response.status = 500;
       response.errors = err;
       response.userMessage = 'server error';
       response.data = null;
       return SendResponse(res);
     } else {
       //send response to client
       response.error = false;
       response.status = 200;
       response.errors = null;
       response.userMessage = 'profile updated';
       response.data = null;
       return SendResponse(res);
     }
   });
 }

 /*-----  End of updateUser  ------*/