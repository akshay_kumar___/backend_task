var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

// name, email, date of birth, one line status
var user = new Schema({
	name: {
		type: String,
		default: ''
	},
	email: {
		type: String,
		default: ''
	},
	password: {
		type: String,
		default: ''
	},
	createdAt: {
		type: Date,
		default: Date.now
	},
	dob: {
		type: String,
		default: ''
	},
	online: {
		type: Boolean,
		default: false
	}
});

var session = new Schema({
	userId: {
		type: ObjectId,
		default: null,
		ref: 'user'
	},
	createdAt: {
		type: Date,
		default: Date.now
	},
	authToken: {
		type: String
	}
});

mongoose.model('user', user);
mongoose.model('session', session);