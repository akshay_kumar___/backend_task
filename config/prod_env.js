/**
 * Expose
 */

module.exports = {
	db: 'mongodb://localhost:27017/task',
	logDir: '/var/log/api/', //@todo : check if log directory exits, if not create one.
	sessionSecret: "thisisareallylongandbigsecrettoken",

};