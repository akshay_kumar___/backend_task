var express = require('express');
var app = express();
var mongoose = require('mongoose');
var config = require('./config/config');
var fs = require('fs');
var port = process.env.PORT || 8090; // set our port

// Connect to mongodb
var connect = function() {
	var options = {
		server: {
			socketOptions: {
				keepAlive: 1
			}
		}
	};
	console.log(config.db);
	mongoose.connect(config.db, options);
};

connect();
mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connect);

// Bootstrap models
fs.readdirSync(__dirname + '/app/models').forEach(function(file) {
	if (~file.indexOf('.js')) require(__dirname + '/app/models/' + file);
});


// Bootstrap application settings
require('./config/express')(app);


// Bootstrap routes
var router = express.Router();
require('./config/routes')(router);
app.use('/api', router);

var server = app.listen(port);

console.log('API started, Assigned port : ' + port);


module.exports = app;